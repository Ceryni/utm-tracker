<?php

// Show Custom Fields box on orders page
add_filter('acf/settings/remove_wp_meta_box', '__return_false');

/**
 * Adds 'UTM' column header to 'Orders' page immediately after 'Total' column.
 *
 * @param string[] $columns
 * @return string[] $new_columns
 */
function add_utm_column_header( $columns ) {
    $new_columns = array();
    foreach ( $columns as $column_name => $column_info ) {
        $new_columns[ $column_name ] = $column_info;
        if ( 'order_total' === $column_name ) {
            $new_columns['utm'] = __( 'UTM', 'my-textdomain' );
        }
    }
    return $new_columns;
}
add_filter( 'manage_edit-shop_order_columns', 'add_utm_column_header', 20 );



if ( ! function_exists( 'pull_meta_data' ) ) :
    /**
     * Helper function to get meta for an order.
     *
     * @param \WC_Order $order the order object
     * @param string $key the meta key
     * @param bool $single whether to get the meta as a single item. Defaults to `true`
     * @param string $context if 'view' then the value will be filtered
     * @return mixed the order property
     */
    function pull_meta_data( $order, $key = '', $single = true, $context = 'edit' ) {

        // WooCommerce > 3.0
        if ( defined( 'WC_VERSION' ) && WC_VERSION && version_compare( WC_VERSION, '3.0', '>=' ) ) {
            $value = $order->get_meta( $key, $single, $context );
        } else {
            // have the $order->get_id() check here just in case the WC_VERSION isn't defined correctly
            $order_id = is_callable( array( $order, 'get_id' ) ) ? $order->get_id() : $order->id;
            $value    = get_post_meta( $order_id, $key, $single );
        }
        return $value;
    }
endif;


/**
 * Get conversion time
 */
function utm_conversion_time ($post) {
    $order   = wc_get_order( $post->ID );
    // get the converstion start time
    $utm_conversion_start_milli = get_post_meta( $order->get_id(), 'utm_conversion_start', true );
    // remove the last 3 digits
    $utm_conversion_start_human = date("H:i:s", substr($utm_conversion_start_milli, 0, 10));
    // get the order time
    $order_date = $order->order_date;
    // convert the order time to milliseconds
    $utm_conversion_end_milli = strtotime($order_date) * 1000;
    // rmeove the last 3 digits
    $utm_conversion_end_milli = substr($utm_conversion_end_milli, 0, -3);
    $utm_conversion_end_human = date("H:i:s", $utm_conversion_end_milli);

    $time1 = new DateTime($utm_conversion_end_human);
    $time2 = new DateTime($utm_conversion_start_human);
    $interval = $time1->diff($time2);
    $interval_human = $interval->format('<strong>%d</strong> day(s) <strong>%h</strong> hour(s) <strong>%i</strong> minute(s) <strong>%s</strong> second(s)');

    return $interval_human;
}


/**
 * Adds 'UTM' column content to 'Orders' page immediately after 'Total' column.
 *
 * @param string[] $column name of column being displayed
 */
function add_utm_column_content( $column ) {
    global $post;
    if ( 'utm' === $column ) {

        $order    = wc_get_order( $post->ID );
		$utm_medium = get_post_meta( $order->get_id(), 'utm_medium', true );
		$utm_source = get_post_meta( $order->get_id(), 'utm_source', true );
		$utm_campaign = get_post_meta( $order->get_id(), 'utm_campaign', true );
        $utm_referrer = get_post_meta( $order->get_id(), 'utm_referrer', true );

        // get the converstion start time
        $utm_conversion_start_milli = get_post_meta( $order->get_id(), 'utm_conversion_start', true );

        ?>
        <?php if( $utm_medium || $utm_source ||  $utm_campaign) : ?>
			<p><strong>Medium:</strong> <?php echo $utm_medium; ?></p>
			<p><strong>Source:</strong> <?php echo $utm_source ?></p>
			<p><strong>Campaign:</strong> <?php echo ( $utm_campaign ) ?></p>
		<?php endif;?>
        <?php if( $utm_referrer) : ?>
			<p><strong>Referrer:</strong> <?php echo $utm_referrer; ?> <a target="_blank" href="<?php echo $utm_referrer; ?>"><span style="font-size: 14px; top: 2px; position: relative;" class="dashicons dashicons-admin-links"></span></a></p>
		<?php endif;?>

        <?php if($utm_conversion_start_milli):?>
            <!-- <p><strong>Conversion Start Time Milli:</strong> <?php echo $utm_conversion_start_milli;?></p> -->
            <!-- <p><strong>Conversion Start Time:</strong> <?php echo $utm_conversion_start_human;?></p> -->
            <!-- <p><strong>Conversion End Time Milli:</strong> <?php echo $utm_conversion_end_milli;?></p> -->
            <!-- <p><strong>Conversion End Time:</strong> <?php echo $utm_conversion_end_human;?></p> -->
            <p><strong>Conversion Time:</strong> <?php echo utm_conversion_time($post);?> </p>
        <?php endif;?>
        <?php

    }
}
add_action( 'manage_shop_order_posts_custom_column', 'add_utm_column_content' );



/**
 * Adjusts the styles for the new 'UTM' column.
 */
function utm_column_size() {
    $css = '.column-utm { width: 30%!important; }';
    wp_add_inline_style( 'woocommerce_admin_styles', $css );
}
add_action( 'admin_print_styles', 'utm_column_size' );



/**
 * Add the field to the checkout (hidden in js)
 */
add_action( 'woocommerce_after_order_notes', 'utm_conversion_checkout_field' );

function utm_conversion_checkout_field( $checkout ) {

    echo '<div id="utm_conversion_checkout_field"><h2>' . __('UTM Conversion') . '</h2>';

    woocommerce_form_field( 'utm_conversion_summary', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => __('Fill in this field'),
        'placeholder'   => __('UTM Conversion time'),
    ), $checkout->get_value( 'utm_conversion_summary' ));

    echo '</div>';

}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'utm_conversion_value' );

function utm_conversion_value( $order_id ) {
    if ( ! empty( $_POST['utm_conversion_summary'] ) ) {

        $order = new WC_Order( $order_id );
        update_post_meta( $order_id, 'utm_conversion_summary', utm_conversion_time($order));
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'utm_conversion_admin_value', 10, 1 );

function utm_conversion_admin_value($order){
    echo '<p><strong>'.__('UTM Conversion').':</strong> ' . get_post_meta( $order->id, 'utm_conversion_summary', true ) . '</p>';
}