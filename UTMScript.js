/**
* Creates a new class UTMPass
* @class Represents a UTM tracker
*/

class UTMPass {
    /*
    * Initialises all delegates
    */
    initialise() {
        this.UTMCall();
        this.loadUTMScript();
    }

    UTMCall() {
        console.log(
            '%c UTM Tracking has been found and loaded!',
            'background: #9C27B0; color: #ffffff; padding: 5px; border-radius: 10px; font-weight: bold'
        );
    }

    loadUTMScript(){

        // catch all the inputs we need
        let utmTracking = [
            '#utm_source',
            '#utm_campaign',
            '#utm_medium',
            '#utm_referrer',
            '#utm_conversion_start',
        ];


        // target the hidden fileds
        $.each(utmTracking, (index, value) => {
            // if any of these exis anywhere on the page
            if($(`${value}`).length) {
                // change input type to hidden
                $(`${value}`).get(0).type = 'hidden';
            }
        });

        if($('#utm_conversion_summary').length) {
            $('#utm_conversion_summary').val('time');
            $('#utm_conversion_summary').get(0).type = 'hidden';
        }


        // start the process
        const utmQuery = settings => {
            // reset the form
            let reset = settings && settings.reset ? settings.reset : false;
            console.log("reset: ", reset)
            // get the location string
            let self = window.location.toString();
            // split the string
            let querystring = self.split("?");
            console.log("querystring: ", querystring)
            // get the referring url
            let referrer =  document.referrer;
            console.log("referrer: ", referrer)
            // get conversion start time using moment.js getting UTC
            let conversion_start = moment().utc();
            // trim the 3 last digits to get seconds
            conversion_start = Math.floor(conversion_start / 1000)
            // get set conversion time
            console.log("conversion_start from session", sessionStorage.getItem('utm_conversion_start'))
            // if we have more than one param
            if (querystring.length > 1) {
                // if we dont already have a referral
                if (reset || sessionStorage.getItem('utm_referrer') === null) {
                    // set the referrer seperately
                    sessionStorage.setItem('utm_referrer', referrer)
                }
                // if we dont already have a conversion star time
                if (reset || sessionStorage.getItem('utm_conversion_start') === null) {
                    // set the referrer seperately
                    sessionStorage.setItem('utm_conversion_start', conversion_start)
                }
                // split it at the apmersand
                let pairs = querystring[1].split("&");
                console.log("pairs: ", pairs)
                // initialise i
                let i = 0;
                // for every 1 in pairs
                for (i in pairs) {
                    // split with an =
                    let utmParams = pairs[i].split("=");
                    // if no session is already set
                    if (reset || sessionStorage.getItem(utmParams[0]) === null) {
                        console.log("utmParams: ", utmParams)
                        // set the paramters in the session
                        sessionStorage.setItem(utmParams[0], decodeURIComponent(utmParams[1]));
                    }
                }
            }

            //let hiddenFields = document.querySelectorAll(utmTracking);
            let hiddenFields = document.querySelectorAll(utmTracking);
            console.log("hiddenFields: ", hiddenFields)

            // for every hidden field
            for (let i = 0; i < hiddenFields.length; i++) {
                // get the name of the field
                let param = sessionStorage.getItem(hiddenFields[i].name)
                console.log("param: ", param)
                // input the param
                if (param) document.getElementsByName(hiddenFields[i].name)[0].value = param;
            }
        };

        utmQuery({
            reset: true
        });
    }
}

//http://lifesaver.test/?utm_source=website&utm_medium=cpc&utm_campaign=Emergency%20Preparedness

export default new UTMPass();
